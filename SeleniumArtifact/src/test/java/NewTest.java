import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
//import io.github.bonigarcia.wdm.WebDriverManager;

import io.github.bonigarcia.wdm.WebDriverManager;

/*import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;*/

public class NewTest {
	public String baseUrl = "https://www.testandquiz.com/selenium/testing.html";  
	
	public WebDriver driver ; 
	
	@Before  
	public void beforeTest() throws IOException, InterruptedException {    
		System.out.println("Booting Driver");
		/*
		 * Process p = Runtime.getRuntime().
		 * exec("chmod 600 'System.getProperty(\"user.dir\")+\"/src/test/resources/chromedriver.exe\""
		 * ); p.wait(50000); p.exitValue();
		 */	
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/resources/chromedriver.exe");  
		// Create driver object for CHROME browser 
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-debugging-port=9222");
		options.addArguments("--disable-dev-shm-usage");
//		options.addArguments("no-sandbox");
//		options.addArguments("--disable-extensions");
//		options.addArguments("--headless");
		// driver = new RemoteWebDriver(new URL("http://selenium__standalone-chrome:4444/wd/hub"), options);
		       // WebDriverManager.chromedriver().setup();
		        
//        WebDriverManager.seleniumServerStandalone().setup();
//       
//
		driver = new ChromeDriver(options);  
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);  
		driver.manage().window().maximize();  
		driver.get(baseUrl);
	}
	
	@Test
	public void test() {
		System.out.println("Starting Test");
		// get the current URL of the page  
		String URL= driver.getCurrentUrl();  
		System.out.println("URL:	" + URL);  
		//get the title of the page  
		String title = driver.getTitle();                  
		System.out.println("Title	" + title);
		//test script
		String text = driver.findElement(By.xpath("/html/body/div/div[3]/div/b")).getText();
		System.out.println("Text	" + text);
		driver.findElement(By.xpath("/html/body/div/div[4]/div/p/a")).click();
		driver.navigate().back();
		driver.findElement(By.id("fname")).sendKeys("Arpan Kushwaha");
		driver.findElement(By.id("idOfButton")).click();
		driver.findElement(By.id("female")).click();
		driver.findElement(By.cssSelector("input.Automation")).click();
		Select dropdown = new Select(driver.findElement(By.id("testingDropdown")));
		dropdown.selectByValue("Manual");
	}      
	
	
//	@Test
//	public void test1() {
//		System.out.println("Starting Test");
//		// get the current URL of the page  
//		String URL= driver.getCurrentUrl();  
//		System.out.println("URL:	" + URL);  
//		//get the title of the page  
//		String title = driver.getTitle();                  
//		System.out.println("Title	" + title);
//		//test script
//		String text = driver.findElement(By.xpath("/html/body/div/div[3]/div/b")).getText();
//		System.out.println("Text	" + text);
//		driver.findElement(By.xpath("/html/body/div/div[4]/div/p/a")).click();
//		driver.navigate().back();
//		driver.findElement(By.id("fname")).sendKeys("Arpan Kushwaha");
//		driver.findElement(By.id("idOfButton")).click();
//		driver.findElement(By.id("female")).click();
//		driver.findElement(By.cssSelector("input.Automation")).click();
//		Select dropdown = new Select(driver.findElement(By.id("testingDropdown")));
//		dropdown.selectByValue("Manual");
//	}      
//	
//	@Test
//	public void test2() {
//		System.out.println("Starting Test");
//		// get the current URL of the page  
//		String URL= driver.getCurrentUrl();  
//		System.out.println("URL:	" + URL);  
//		//get the title of the page  
//		String title = driver.getTitle();                  
//		System.out.println("Title	" + title);
//		//test script
//		String text = driver.findElement(By.xpath("/html/body/div/div[3]/div/b")).getText();
//		System.out.println("Text	" + text);
//		driver.findElement(By.xpath("/html/body/div/div[4]/div/p/a")).click();
//		driver.navigate().back();
//		driver.findElement(By.id("fname")).sendKeys("Arpan Kushwaha");
//		driver.findElement(By.id("idOfButton")).click();
//		driver.findElement(By.id("female")).click();
//		driver.findElement(By.cssSelector("input.Automation")).click();
//		Select dropdown = new Select(driver.findElement(By.id("testingDropdown")));
//		dropdown.selectByValue("Manual");
//	}      
	@After  
	public void afterTest() {  
		System.out.println("Quitting Driver");  
		driver.quit();  
	}
	
}
  
